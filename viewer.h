
#include <array>
#include <vector>

namespace interview {

void showMesh(std::vector<std::array<double, 3>> vertexPositions, std::vector<std::vector<size_t>> faceIndices);

void loadMesh(std::string filename, std::vector<std::array<double, 3>>& vertexPositions, std::vector<std::vector<size_t>>& faceIndices);

template<typename Position>
void translateMesh(std::vector<Position>& vertexPositions, const Position& offset);
}