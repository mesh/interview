#include "polyscope/polyscope.h"
#include "polyscope/gl/ground_plane.h"
#include "polyscope/surface_mesh.h"
#include "polyscope/surface_mesh_io.h"

#include "viewer.h"

namespace interview {
void showMesh(std::vector<std::array<double, 3>> vertexPositions, std::vector<std::vector<size_t>> faceIndices) {
    // Initialize viewer
  polyscope::init();
  polyscope::gl::groundPlaneEnabled = false;
  auto psmesh = polyscope::registerSurfaceMesh("some mesh", vertexPositions, faceIndices);
  psmesh->edgeWidth = 1.0;
  polyscope::show();
}

void loadMesh(std::string filename, std::vector<std::array<double, 3>>& vertexPositions, std::vector<std::vector<size_t>>& faceIndices) {
  polyscope::loadPolygonSoup_OBJ(filename, vertexPositions, faceIndices);
}

template<typename Position>
void translateMesh(std::vector<Position>& vertexPositions, const Position& offset) {
  for(auto& p : vertexPositions) {
    for(size_t i = 0; i < offset.size(); i++) {
      p[i] += offset[i];
    }
  }
}

}

// Template instantiations
template void interview::translateMesh<std::__1::array<float, 3ul> >(std::__1::vector<std::__1::array<float, 3ul>, std::__1::allocator<std::__1::array<float, 3ul> > >&, std::__1::array<float, 3ul> const&);