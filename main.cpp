#include "viewer.h"

#include <string>

int main(int argc, char** argv) {
  std::string filename = "../data/subdivided_plane.obj";

  std::vector<std::array<double, 3>> vertexPositions;
  std::vector<std::vector<size_t>> faceIndices;
  interview::loadMesh(filename, vertexPositions, faceIndices);

  // interview::translateMesh(vertexPositions, std::array<double, 3>{0,0,1});
  interview::showMesh(vertexPositions, faceIndices);
  return 0;
}